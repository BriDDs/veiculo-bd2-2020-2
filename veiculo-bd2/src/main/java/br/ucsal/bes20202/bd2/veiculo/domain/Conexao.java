package br.ucsal.bes20202.bd2.veiculo.domain;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Conexao {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("veiculobd2");
		EntityManager em = emf.createEntityManager();

		Veiculo veiculo = new Veiculo("C2077", TipoVeiculoEnum.PASSEIO, "N77");

		em.getTransaction().begin();
		em.merge(veiculo);
		em.getTransaction().commit();

		System.out.println("em.contains(veiculo)=" + em.contains(veiculo));
		System.out.println("Tipo = " + veiculo.getTipo().getSigla());
		System.out.println("Chassi = " + veiculo.getChassis());
		emf.close();

	}

}
