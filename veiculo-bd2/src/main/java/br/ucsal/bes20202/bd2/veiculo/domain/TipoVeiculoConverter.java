package br.ucsal.bes20202.bd2.veiculo.domain;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class TipoVeiculoConverter implements AttributeConverter<TipoVeiculoEnum, String>{

	@Override
	public String convertToDatabaseColumn(TipoVeiculoEnum attribute) {
		return attribute == null ? null : attribute.getSigla();
	}

	@Override
	public TipoVeiculoEnum convertToEntityAttribute(String dbData) {
		return dbData == null ? null : TipoVeiculoEnum.valueOfsigla(dbData);
	}

}
